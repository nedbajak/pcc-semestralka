**Semestrální práce: Hra Snake**


**Popis Zadání**
- Tento projekt představuje implementaci klasické hry Snake. Hra je navržena pro spuštění v terminálovém okně. Cílem hry je ovládat hada, který se pohybuje po herní ploše, a snažit se sníst co nejvíce ovoce, čímž se had prodlužuje a hráč získává body. Hra končí, pokud se had střetne sám se sebou. Po dokončení hry se uživateli vypíše dosažené skóre a čas za který to odehrál.

**Popis Implementace** 
- Hra je implementována jako vícevláknová aplikace v C++. 

**Vlákna** : InputThread(řeší vstupy od uživatele), MainThread(GameLoop,Logika a vykreslování), TimerThread(Počítá odehraný čas).

Hlavní funkce hry zahrnuje:

- Logiku hry: Kontroluje pohyb hada, detekci kolizí a skórování.

- Ovládání hada: Implementováno ve třídě InputHandler, která čte uživatelský vstup z klávesnice ve vlastním vlákně.
- Časování: Třída Timer je použita pro měření času uplynulého od začátku hry.

**Funkčnost a Ovládání Aplikace**

Hra je ovládána pomocí kláves:


- W pro pohyb hada nahoru.

- S pro pohyb hada dolů.

- A pro pohyb hada doleva.

- D pro pohyb hada doprava.
- U pro okamžité ukončení hry.

Pro spuštění hry stačí spustit sestavený spustitelný soubor v terminálovém okně. Hra obsahuje také argument --help, který zobrazí informace o ovládání a pravidlech hry.


**Dr. Memory**

Po spuštění Dr.Memory jsem dostal tento výsledek :

![Result](drmemoryresult.png)

Na internetu jsem se dohledal informace, že jsou tyto problémy zaviněné tím, že používám globální proměnné, které jsou deklarované v Globals.h. S výsledkem jsem mimo to spokojený.
