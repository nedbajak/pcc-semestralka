#ifndef SNAKE_TIMER_H
#define SNAKE_TIMER_H

#include <chrono>
#include <thread>
#include <atomic>

class Timer {
public:
    Timer();
    ~Timer();
    void start();
    void stop();
    int getSecondsElapsed() const;

private:
    std::atomic<bool> running;
    std::atomic<int> secondsElapsed;
    std::thread timerThread;
    void run();
};

#endif //SNAKE_TIMER_H
