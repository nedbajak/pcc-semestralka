//
// Created by nedba on 10.01.2024.
//

#ifndef SNAKE_GLOBALS_H
#define SNAKE_GLOBALS_H

#include "Timer.h"

enum eDirection { STOP = 0, LEFT, RIGHT, UP, DOWN};

extern bool gameOver;
extern int x, y, fruitX, fruitY, score;
extern int tailX[100], tailY[100];
extern int nTail;
extern eDirection dir;
extern Timer gameTimer;
#endif //SNAKE_GLOBALS_H
