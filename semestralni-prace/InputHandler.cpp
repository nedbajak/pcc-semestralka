//
// Created by nedba on 10.01.2024.
//

#include "InputHandler.h"
#include <iostream>
#include <conio.h>
#include <windows.h>
//
// Created by nedba on 10.01.2024.
//


void InputHandler::operator()() {
while (!gameOver){
    if (_kbhit()) {
        switch (_getch()) {
            case 'a':
                dir = LEFT;
                break;
            case 'd':
                dir = RIGHT;
                break;
            case 'w':
                dir = UP;
                break;
            case 's':
                dir = DOWN;
                break;
            case 'u':
                gameOver = true;
                break;
        }
    }
}

}
