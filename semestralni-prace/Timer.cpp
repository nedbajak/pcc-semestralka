#include "Timer.h"

Timer::Timer() : running(false), secondsElapsed(0) {}

Timer::~Timer() {
    stop();
}

void Timer::start() {
    running = true;
    timerThread = std::thread(&Timer::run, this);
}

void Timer::stop() {
    running = false;
    if (timerThread.joinable()) {
        timerThread.join();
    }
}

int Timer::getSecondsElapsed() const {
    return secondsElapsed;
}

void Timer::run() {
    while (running) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        secondsElapsed++;
    }
}
