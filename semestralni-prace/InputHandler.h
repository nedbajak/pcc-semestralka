//
// Created by nedba on 10.01.2024.
//
#include "Globals.h"

#ifndef SNAKE_INPUTHANDLER_H
#define SNAKE_INPUTHANDLER_H


class InputHandler {
public:
    void operator()();

};


#endif //SNAKE_INPUTHANDLER_H
