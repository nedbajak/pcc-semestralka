
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <thread>
#include <sstream>
#include <iomanip>
#include "InputHandler.h"
#include "Globals.h"
#include "Timer.h"
using namespace std;

bool gameOver;
const int width = 20;
const int height = 17;
int x = 0;
int y = 0;
int fruitX = 0;
int fruitY = 0;
int score = 0;
int tailX[100], tailY[100];
int nTail = 0;
eDirection dir;
Timer gameTimer; // Create an instance of Timer


void Setup() {
    gameOver = false;
    dir = STOP;
    x = width / 2;
    y = height / 2;
    fruitX = rand() % width;
    fruitY = rand() % height;
    score = 0;
}

using namespace std;

// Funkce pro zapnutí raw módu
void enableRawMode() {
    HANDLE hstdin = GetStdHandle(STD_INPUT_HANDLE);
    DWORD mode;
    GetConsoleMode(hstdin, &mode);
    mode &= ~(ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT); // Vypne echo a line input
    SetConsoleMode(hstdin, mode);
}

// Funkce pro vypnutí raw módu
void disableRawMode() {
    HANDLE hstdin = GetStdHandle(STD_INPUT_HANDLE);
    DWORD mode;
    GetConsoleMode(hstdin, &mode);
    mode |= (ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT); // Zapne echo a line input
    SetConsoleMode(hstdin, mode);
}

// Funkce pro zapnutí ANSI escape kódů
void enableANSIEscapeCodes() {
    HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
    DWORD mode;
    GetConsoleMode(hstdout, &mode);
    mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    SetConsoleMode(hstdout, mode);
}


void DrawBuffered() {
    stringstream buffer;

    // Vymaže obrazovku a přesune kurzor na počáteční pozici pomocí ANSI escape sekvencí
    buffer << "\x1b[2J\x1b[H";

    // Horní okraj
    buffer << string(width + 2, '#') << "\n";

    // Herní plocha
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            if (j == 0)
                buffer << "#"; // Levý okraj

            if (i == y && j == x)
                buffer << "O"; // Hlava hada
            else if (i == fruitY && j == fruitX)
                buffer << "F"; // Ovoce
            else {
                bool print = false;
                for (int k = 0; k < nTail; k++) {
                    if (tailX[k] == j && tailY[k] == i) {
                        buffer << "o"; // Ocas hada
                        print = true;
                        break;
                    }
                }
                if (!print) buffer << " ";
            }

            if (j == width - 1)
                buffer << "#"; // Pravý okraj
        }
        buffer << "\n";
    }

    // Dolní okraj
    buffer << string(width + 2, '#') << "\n";

    // Skóre a časovač
    int seconds = gameTimer.getSecondsElapsed();
    int minutes = seconds / 60;
    seconds %= 60;
    buffer << "Cas: " << setw(2) << setfill('0') << minutes << ":" << setw(2) << seconds << " | ";
    buffer << "Skore: " << score << "\n";

    // Vypíše obsah bufferu na konzoli
    cout << buffer.str();
}



void Logic() {
    int prevX = tailX[0];
    int prevY = tailY[0];
    int prev2X, prev2Y;
    tailX[0] = x;
    tailY[0] = y;
    for (int i = 1; i < nTail; i++) {
        prev2X = tailX[i];
        prev2Y = tailY[i];
        tailX[i] = prevX;
        tailY[i] = prevY;
        prevX = prev2X;
        prevY = prev2Y;
    }
    switch (dir) {
        case LEFT:
            x--;
            break;
        case RIGHT:
            x++;
            break;
        case UP:
            y--;
            break;
        case DOWN:
            y++;
            break;
        default:
            break;
    }
    if (x >= width) x = 0; else if (x < 0) x = width - 1;
    if (y >= height) y = 0; else if (y < 0) y = height - 1;

    for (int i = 0; i < nTail; i++)
        if (tailX[i] == x && tailY[i] == y)
            gameOver = true;

    if (x == fruitX && y == fruitY) {
        score += 10;
        fruitX = rand() % width;
        fruitY = rand() % height;
        nTail++;
    }
}

int main(int argc, char *argv[]) {
    // Hleda --help argument
    if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        cout << "Hra Snake\n";
        cout << "Toto je implementace klasické hry Snake v C++.\n";
        cout << "Hra se hraje v terminálovém okně. Pro ovládání hada použijte klávesy:\n";
        cout << "  W - nahoru\n";
        cout << "  S - dolů\n";
        cout << "  A - doleva\n";
        cout << "  D - doprava\n";
        cout << "Klávesou U hru ukončíte.\n";
        cout << "Cílem hry je sníst co nejvíce ovoce (znázorněné písmenem F), což zvýší vaše skóre.\n";
        cout << "Každé snězené ovoce prodlouží hada o jedno pole.\n";
        cout << "Hra končí, když se had srazí se svým vlastním tělem.\n";
        return 0;
    }

    enableRawMode();
    enableANSIEscapeCodes();
    Setup();

    InputHandler inputHandler;
    thread inputThread(inputHandler);

    gameTimer.start(); // Spusti timer

    while (!gameOver) {
        DrawBuffered();
        Logic();
        Sleep(150); // Zpomali hru pro lepsi hratelnost
    }

    inputThread.join();
    gameTimer.stop(); // Zastavi timer

    disableRawMode();

    int finalSeconds = gameTimer.getSecondsElapsed();
    int finalMinutes = finalSeconds / 60;
    finalSeconds %= 60;
    cout << endl;
    cout <<"------" <<"|GAME OVER|" << "------"<< endl;
    cout << endl;

    cout << "Finalni skore: " << score << endl;
    cout << "Celkovy cas: " << setw(2) << setfill('0') << finalMinutes << ":" << setw(2) << finalSeconds-1 << endl;

    cout << "Pro ukonceni stisknete libovolnou klavesu..." << endl;
    _getch(); // Cekani na stisknuti
    return 0;
}

